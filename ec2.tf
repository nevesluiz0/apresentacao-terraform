data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]

  }

   owners = ["099720109477"]
   
}

resource "aws_instance" "Grupo2" {
   ami           = data.aws_ami.ubuntu.id
   instance_type = var.instance_type
   key_name = var.chave 
   subnet_id = var.subnet_public_id
   vpc_security_group_ids = [data.aws_security_group.default.id]
   //aws_iam_role = ["SSM-Dev-Trilha"]
   iam_instance_profile = data.aws_iam_role.SSM-Dev-Trilha.id
   associate_public_ip_address = false
   tags = {
     Name = var.name
  }

}

data "aws_security_group" "default" {

 filter {
    name   = "group-name"
    values = ["default"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

}

data "aws_iam_role" "SSM-Dev-Trilha" {

  name = "SSM-Dev-Trilha"

}
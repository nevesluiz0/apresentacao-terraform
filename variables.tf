variable "region" {
  description = "Define what region the instance will be deployed"
  default = "us-east-1"
}

variable "name" {
  description = "Name of the Application"
  default = "Grupo-2"
}

variable "env" {
  description = "Environment of the Application"
  default = "dev"
}

variable "instance_type" {
  description = "AWS Instance type defines the hardware configuration of the machine"
  default = "t2.micro"
}

variable "vpc_id" {
  default = "vpc-091f83f67d214ac96" #VPC ID .

}

variable "subnet_public_id" {
  default = "subnet-065f678fc11f9f17d" #Subnet ID .

}
variable "chave" {
  default = "lucas-soazem-key" #Subnet ID .

}




